val akkaVersion = "2.4.16"

val dependencies = Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "org.asteriskjava"  %  "asterisk-java" % "2.0.1.a.XIVOCC"
)

val testDependencies = Seq(
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test"
)

lazy val commonSettings = Seq(
  organization := "solutions.xivo",
  version := "0.1.0-SNAPSHOT",
  scalaVersion := "2.11.8",
  scalacOptions ++= Seq(
    "-target:jvm-1.8",
    "-encoding", "UTF-8",
    "-unchecked",
    "-deprecation",
    "-feature",
    "-language:existentials",
    "-language:higherKinds",
    "-language:implicitConversions",
    "-Xfuture",
    "-Yno-adapted-args",
    "-Ywarn-dead-code",
    "-Ywarn-numeric-widen",
    "-Ywarn-value-discard",
    "-Ywarn-unused"
  )

)

lazy val root = (project in file("."))
  .settings(
    name := "ami-test",
    commonSettings,
    libraryDependencies ++= dependencies ++ testDependencies,
    parallelExecution in Global := false,
    resolvers += Resolver.mavenLocal
  )
