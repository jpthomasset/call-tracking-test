import org.scalatest._

import GraphBuilder._

class AsteriskGraphSpec extends WordSpec with Matchers {

  
  "AsteriskGraph" should {

    "store links between channels and bridge" in {
      // Checks compilation only
      AsteriskGraph(
        Channel("c1") ~ Bridge("b") ~ Channel("c2")
      )
    }

    "retrieve connected channels of a given channel" in {
      val graph = AsteriskGraph(
        Channel("c1") ~ Bridge("b") ~ Channel("c2")
      )

      graph.remoteChannels(Channel("c1")) should contain only (Channel("c2"))
      graph.remoteChannels(Channel("c2")) should contain only (Channel("c1"))
    }

    "retrieve connected channels of a given channel without mixing paths" in {
      val graph = AsteriskGraph(
        Channel("c1") ~ Bridge("b1") ~ Channel("c2"),
        Channel("c3") ~ Bridge("b2") ~ Channel("c4")
      )

      graph.remoteChannels(Channel("c1")) should contain only (Channel("c2"))
      graph.remoteChannels(Channel("c2")) should contain only (Channel("c1"))

      graph.remoteChannels(Channel("c3")) should contain only (Channel("c4"))
      graph.remoteChannels(Channel("c4")) should contain only (Channel("c3"))
    }

    "remove a channel from a bridge" in {
      val graph = AsteriskGraph(
        Channel("c1") ~ Bridge("b") ~ Channel("c2")
      )

      AsteriskGraph.del(graph, Channel("c1") ~ Bridge("b"))
    }

    "track channel optimization" in {
      val c1 = Channel("c1")
      val c2 = Channel("c2")
      val lc1 = LocalChannel("lc1")
      val lc2 = LocalChannel("lc2")
      val b1 = Bridge("b1")
      val b2 = Bridge("b2")
      val lb = LocalBridge(lc1, lc2)

      val graph = AsteriskGraph(
        c1 ~ b1 ~ lc1 ~ lb ~ lc2 ~ b2 ~ c2 
      )

      graph.remoteChannels(c1) should contain only (c2)
      val optimized = AsteriskGraph.del(graph,
        b1 ~ lc1,
        lc1 ~ lb,
        lb ~ lc2,
        b2 ~ lc2,
        c2 ~ b2).add(c2 ~ b1)

      optimized.remoteChannels(c1) should contain only (c2)
    }

    "draw representation of simple graph" in {
      val graph = AsteriskGraph(
        Channel("c1") ~ Bridge("b") ~ Channel("c2")
      )

      val str = graph.prettyPrint()

      str should be("""Channel(c1) ─ Bridge(b) ─ Channel(c2)""")
    }

    "draw representation of two simple paths" in {
      val graph = AsteriskGraph(
        Channel("c1") ~ Bridge("b1") ~ Channel("c2"),
        Channel("c3") ~ Bridge("b2") ~ Channel("c4")
      )

      val str = graph.prettyPrint()

     str should be("""Channel(c1) ─ Bridge(b1) ─ Channel(c2)
                     |Channel(c3) ─ Bridge(b2) ─ Channel(c4)""".stripMargin)
    }

    "draw representation of complex graph" in {
      val graph = AsteriskGraph(
        Channel("c1") ~ Bridge("b") ~ Channel("c2"),
                        Bridge("b") ~ Channel("c3"),
                        Bridge("b") ~ Channel("c6"),
        Channel("c4") ~ Bridge("b2") ~ Channel("c5")
      )

      val str = graph.prettyPrint()

      str should be("""Channel(c1) ─ Bridge(b) ┬ Channel(c2)
                      |                        ├ Channel(c3)
                      |                        └ Channel(c6)
                      |Channel(c4) ─ Bridge(b2) ─ Channel(c5)""".stripMargin)
    }

    "draw representation of graph with different path length" in {
      val graph = AsteriskGraph(
        Channel("c1") ~ Bridge("b") ~ Channel("c2") ~ Bridge("b3") ~ Channel("c3"),
                        Bridge("b") ~ Channel("c6"),
        Channel("c4") ~ Bridge("b2") ~ Channel("c5")
      )

      val str = graph.prettyPrint()

      str should be("""Channel(c1) ─ Bridge(b) ┬ Channel(c2) ─ Bridge(b3) ─ Channel(c3)
                      |                        └ Channel(c6)
                      |Channel(c4) ─ Bridge(b2) ─ Channel(c5)""".stripMargin)
    }

    "draw representation of graph with multiple split" in {
      val graph = AsteriskGraph(
        Channel("c1") ~ Bridge("b") ~ Channel("c2") ~ Bridge("b3") ~ Channel("c3"),
                                                      Bridge("b3") ~ Channel("c7"),
                        Bridge("b") ~ Channel("c6"),
        Channel("c4") ~ Bridge("b2") ~ Channel("c5")
      )

      val str = graph.prettyPrint()

      str should be("""Channel(c1) ─ Bridge(b) ┬ Channel(c2) ─ Bridge(b3) ┬ Channel(c3)
                      |                        │                          └ Channel(c7)
                      |                        └ Channel(c6)
                      |Channel(c4) ─ Bridge(b2) ─ Channel(c5)""".stripMargin)
    }

  }

}


