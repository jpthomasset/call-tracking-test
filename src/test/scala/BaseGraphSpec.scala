import org.scalatest._

import BaseGraph._
import GraphBuilder._


object BaseGraphTest extends GraphBuilder[String, BaseGraphTest]{
  implicit def newBuilder(links: Links[String]): BaseGraphTest = new BaseGraphTest(links)
}

class BaseGraphTest(links: Links[String]) extends BaseGraph[String, BaseGraphTest](links) {
  def innerLinks = links
}

class BaseGraphSpec extends WordSpec with Matchers {

  
  "BaseGraph" should {

    "store simple edge" in {
      val graph = BaseGraphTest("a" ~ "b")

      graph.neighbors("a") should contain ("b")
      graph.neighbors("b") should contain ("a")
    }

    "store separated edges" in {
      val graph = BaseGraphTest(
        "a" ~ "b",
        "c" ~ "d"
      )

      graph.neighbors("a") should not contain ("c")
      graph.neighbors("a") should not contain ("d")
    }

    "remove edge" in {
      val graph = BaseGraphTest("a" ~ "b")

      graph.del("a" ~ "b").neighbors("a") shouldBe empty
      graph.del("a" ~ "b").neighbors("b") shouldBe empty
    }

    "remove edge whatever the order of nodes" in {
      val graph = BaseGraphTest("a" ~ "b")

      graph.del("b" ~ "a").neighbors("a") shouldBe empty
      graph.del("b" ~ "a").neighbors("b") shouldBe empty
    }

    "remove one edge from a multi-edge path" in {
      val graph = BaseGraphTest("a" ~ "b" ~ "c")

      graph.del("a" ~ "b").neighbors("a") shouldBe empty
      graph.del("a" ~ "b").neighbors("b") should contain only ("c")
      graph.del("a" ~ "b").neighbors("c") should contain only ("b")
    }

    "remove one edge from a multi-edge path using builder" in {
      val graph = BaseGraphTest("a" ~ "b" ~ "c")

      BaseGraphTest.del(graph, "a" ~ "b").neighbors("a") shouldBe empty
      BaseGraphTest.del(graph, "a" ~ "b").neighbors("b") should contain only ("c")
      BaseGraphTest.del(graph, "a" ~ "b").neighbors("c") should contain only ("b")
    }

    "remove edge and optimize structure" in {
      val graph = BaseGraphTest("a" ~ "b" ~ "c")

      graph.del("a" ~ "b").innerLinks.get("a") should be (None)
      graph.del("a" ~ "b").innerLinks.get("b") should be (Some(Set("c")))
    }

    "get all connections of a node" in {
      val graph = BaseGraphTest(
        "a" ~ "b" ~ "c" ~ "d"
      )

      graph.connectionsOf("a") should contain only ("b", "c", "d")
    }

    "get all connections when there are duplicate path" in {
      val graph = BaseGraphTest(
        "a" ~ "b" ~ "c",
        "a" ~ "d" ~ "c",
              "d" ~ "e"
      )

      graph.connectionsOf("a") should contain only ("b", "c", "d", "e")
    }

    "get all connections when there is a loop" in {
      val graph = BaseGraphTest(
        "a" ~ "b" ~ "c",
        "a" ~ "d" ~ "b"
      )

      graph.connectionsOf("a") should contain only ("b", "c", "d")
    }

    "remove all edges based on a node" in {
      val graph = BaseGraphTest(
        "a" ~ "b" ~ "c",
        "a" ~ "d" ~ "e"
      )

      graph.del("d").connectionsOf("a") should contain only ("b", "c")
    }



  }

}


