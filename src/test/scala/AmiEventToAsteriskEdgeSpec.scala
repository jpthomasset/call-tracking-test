import org.scalatest._
import org.asteriskjava.manager.event._

import GraphBuilder._

class AmiEventToAsteriskEdgeSpec extends WordSpec with Matchers {
  "AmiEventToAsteriskEdgeSpec" should {
    "connect a channel to a bridge when it enters a bridge" in {
      val c1name = "c1"
      val bridgeEvent = new BridgeEnterEvent(this);
      bridgeEvent.setChannel(c1name)
      bridgeEvent.setBridgeUniqueId("b1")
      val graph = AmiEventToAsteriskEdge(AsteriskGraph(), bridgeEvent)

      graph.neighbors(Channel(c1name)) should contain only (Bridge("b1"))
    }

    "connect two channels together when they enter the same bridge" in {
      val c1name = "c1"
      val c2name = "c2"

      val bridgeEvent = new BridgeEnterEvent(this);
      bridgeEvent.setChannel(c1name)
      bridgeEvent.setBridgeUniqueId("b1")

      val graph = AmiEventToAsteriskEdge(AsteriskGraph(), bridgeEvent)

      bridgeEvent.setChannel(c2name)
      val graph2 = AmiEventToAsteriskEdge(graph, bridgeEvent)

      graph2.remoteChannels(Channel(c1name)) should contain only (Channel(c2name))
    }

    "disconnect two channels together when one leave the bridge" in {
      val c1name = "c1"
      val c2name = "c2"

      val bridgeEvent = new BridgeEnterEvent(this);
      bridgeEvent.setChannel(c1name)
      bridgeEvent.setBridgeUniqueId("b1")

      val graph = AmiEventToAsteriskEdge(AsteriskGraph(), bridgeEvent)

      bridgeEvent.setChannel(c2name)
      val graph2 = AmiEventToAsteriskEdge(graph, bridgeEvent)

      val leaveEvent = new BridgeLeaveEvent(this)
      leaveEvent.setChannel(c2name)
      leaveEvent.setBridgeUniqueId("b1")
      val graph3 = AmiEventToAsteriskEdge(graph2, leaveEvent)

      graph3.remoteChannels(Channel(c1name)) shouldBe empty
    }

    "connect two channels together when one dial the other" in {
      val c1name = "c1"
      val c2name = "c2"

      val dialEvent = new DialBeginEvent(this);
      dialEvent.setChannel(c1name)
      dialEvent.setDestination(c2name)

      val graph = AmiEventToAsteriskEdge(AsteriskGraph(), dialEvent)

      graph.remoteChannels(Channel(c1name)) should contain only (Channel(c2name))
    }

    "disconnect two channels together dial ends" in {
      val c1name = "c1"
      val c2name = "c2"

      val dialEvent = new DialBeginEvent(this);
      dialEvent.setChannel(c1name)
      dialEvent.setDestination(c2name)

      val graph = AmiEventToAsteriskEdge(AsteriskGraph(), dialEvent)

      val dialEndEvent = new DialEndEvent(this);
      dialEndEvent.setChannel(c1name)
      dialEndEvent.setDestination(c2name)

      val graph2 = AmiEventToAsteriskEdge(graph, dialEndEvent)

      graph2.remoteChannels(Channel(c1name)) shouldBe empty
    }

    "connect two channels together when bridged with a local bridge" in {
      val c1name = "c1"
      val c2name = "c2"
      val lc1name = "Local/c1"
      val lc2name = "Local/c2"

      val evt = new LocalBridgeEvent(this);
      evt.setLocalOneChannel(lc1name)
      evt.setLocalTwoChannel(lc2name)
      val graph1 = AmiEventToAsteriskEdge(AsteriskGraph(), evt)

      val bridgeEvent = new BridgeEnterEvent(this);
      bridgeEvent.setChannel(c1name)
      bridgeEvent.setBridgeUniqueId("b1")
      val graph2 = AmiEventToAsteriskEdge(graph1, bridgeEvent)

      bridgeEvent.setChannel(lc1name)
      val graph3 = AmiEventToAsteriskEdge(graph2, bridgeEvent)

      bridgeEvent.setChannel(c2name)
      bridgeEvent.setBridgeUniqueId("b2")
      val graph4 = AmiEventToAsteriskEdge(graph3, bridgeEvent)

      bridgeEvent.setChannel(lc2name)
      val graph5 = AmiEventToAsteriskEdge(graph4, bridgeEvent)

      graph5.remoteChannels(Channel(c1name)) should contain only (Channel(c2name))
    }
  }
}
