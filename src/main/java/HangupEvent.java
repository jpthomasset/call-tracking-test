package xuc;
import org.asteriskjava.manager.event.UserEvent;
    
public class HangupEvent extends UserEvent
 {
     private String XIVO_USERUUID;
 
     public HangupEvent(Object source)
     {
         super(source);
     }
 
     public String getXIVO_USERUUID()
     {
         return XIVO_USERUUID;
     }
 
     public void setXIVO_USERUUID(String XIVO_USERUUID)
     {
         this.XIVO_USERUUID = XIVO_USERUUID;
     }
 }
