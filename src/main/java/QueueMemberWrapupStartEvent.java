package xuc;
import org.asteriskjava.manager.event.QueueMemberPausedEvent;
    
public class QueueMemberWrapupStartEvent extends QueueMemberPausedEvent
{
    /**
     * Serial version identifier.
     */
    private static final long serialVersionUID = -6874934715608566919L;
    
    public QueueMemberWrapupStartEvent(Object source)
    {
        super(source);
    }
}
