
import akka.actor.{ Actor, ActorLogging, ActorRef }
import org.asteriskjava.manager.event._
import org.asteriskjava.manager.response.ManagerResponse
import org.asteriskjava.manager.{ManagerConnection, ManagerConnectionFactory, ManagerEventListener, SendActionCallback}
import scala.concurrent.Future

class QueueMemberWrapupStartEvent(source: Object) extends QueueMemberPausedEvent(source)

class AsteriskManagerListener(actor: ActorRef) extends ManagerEventListener with SendActionCallback {
  def onManagerEvent(event: ManagerEvent) = {
    actor ! event
  }
  def onResponse(response: ManagerResponse) = {
    actor ! response
  }
}


class AmiConnector(managerFactory: ManagerConnectionFactory) extends Actor with ActorLogging {

  log.info("Starting AmiConnector")
  val managerListener = new AsteriskManagerListener(self)
  val managerConnection = managerFactory.createManagerConnection()
  managerConnection.addEventListener(managerListener)
  managerConnection.registerUserEventClass(classOf[xuc.HangupEvent])
  managerConnection.registerUserEventClass(classOf[QueueMemberWrapupStartEvent])


  import scala.concurrent.ExecutionContext.Implicits.global
  Future(managerConnection.login())

  var graph = AsteriskGraph()


  def receive = {
    case event: ConnectEvent =>
      log.info("AMI logged on")
      context.become(ready)

    case x =>
      log.info(s"received $x")
  }

  def logGraph() = log.info(s"Graph-> \n\u001B[30;42m${graph.prettyPrint()}\u001B[0m")
  def logString(prefix: String, msg: String) = log.info(s"\u001B[32m${prefix}\u001B[0m -> $msg")
  def logEvent(prefix: String, evt: ManagerEvent) = logString(prefix, evt.toString)
  def logResponse(prefix: String, evt: ManagerResponse) = logString(prefix, evt.toString)
  def logChannelEvent(prefix: String, evt: AbstractChannelEvent) = logEvent(s"${prefix}(${evt.getChannel})", evt)
  def logBridgeEvent(prefix: String, evt: AbstractBridgeEvent) = logEvent(s"${prefix}(${evt.getBridgeUniqueId})", evt)

  def ready: Receive = {
    case evt: NewStateEvent => logChannelEvent("NewState", evt)
    case evt: NewChannelEvent =>
      logChannelEvent("NewChannel", evt)
      //graph = graph + Channel(evt.getChannel)

    case evt: HoldEvent => logEvent("Hold", evt)
    case evt: UnholdEvent => logEvent("Unhold", evt)

      //case evt: NewExtenEvent => log.info(s"$evt")
    case evt: DialEvent =>
      logEvent(s"Dial(${evt.getChannel}, ${evt.getDestination})", evt)
      graph = AmiEventToAsteriskEdge(graph, evt)
      logGraph
      // if(evt.getSubEvent == DialEvent.SUBEVENT_BEGIN) {
      //   graph = graph + Dial(Channel.from(evt.getChannel), Channel.from(evt.getDestination))
      // } else {
      //   //graph -=
      // }
      //graph = graph +
    case evt: xuc.HangupEvent =>
      logEvent("UserEvent Hangup", evt)

    case evt: HangupEvent =>
      logChannelEvent("Hangup", evt)
      graph = AmiEventToAsteriskEdge(graph, evt)
      logGraph

    case evt: AttendedTransferEvent => logEvent("AttendedTransferEvent", evt)
    case evt: BlindTransferEvent => logEvent("BlindTransferEvent", evt)
    case evt: TransferEvent => logEvent("TransferEvent", evt)
      
    case evt: LocalBridgeEvent =>
      logEvent("LocalBridge", evt)
      graph = AmiEventToAsteriskEdge(graph, evt)
      logGraph

    case evt: QueueCallerJoinEvent => logEvent("QueueCallerJoin", evt)
    case evt: QueueCallerLeaveEvent => logEvent("QueueCallerLeave", evt)
    case evt: QueueEvent => logEvent("QueueEvent", evt)

    case evt: MeetMeJoinEvent => logEvent("MeetMeJoinEvent", evt)
    case evt: MeetMeLeaveEvent => logEvent("MeetMeLeaveEvent", evt)

    case evt: DAHDIChannelEvent => logEvent("DAHDIChannelEvent", evt)

    case evt: NewConnectedLineEvent => logEvent("NewConnectedLineEvent", evt)

    case evt: NewCallerIdEvent => logEvent("NewCallerIdEvent", evt)
    case evt: QueueMemberWrapupStartEvent => logEvent("🙂 QueueMemberWrapupStartEvent", evt)
    case evt: QueueMemberPauseEvent => logEvent("🙂 QueueMemberPauseEvent", evt)

    case evt: MonitorStartEvent => logEvent("/!\\ MonitorStartEvent", evt)
    case evt: MonitorStopEvent => logEvent("/!\\ MonitorStopEvent", evt)

    case evt: VarSetEvent =>
      if(Option(evt.getVariable).contains("BRIDGEPEER") || Option(evt.getVariable).filter(_.endsWith("MONITOR_PAUSED")).isDefined)
        logString(s"VarSetEvent(${evt.getChannel})", s"${evt.getVariable}=${evt.getValue}")

    case evt:AbstractBridgeEvent =>
      evt match {
        case e:BridgeEnterEvent => logBridgeEvent(s"BridgeEnterEvent(${e.getChannel})", evt)
        case e:BridgeLeaveEvent => logBridgeEvent(s"BridgeLeaveEvent(${e.getChannel})", evt)
        case _ => logBridgeEvent("Bridge", evt)
      }
      graph = AmiEventToAsteriskEdge(graph, evt)
      logGraph

    //case evt: ManagerEvent => logEvent("ManagerEvent<ABSTRACT>", evt)
    //case evt: ManagerResponse => logResponse("ManagerResponse<ABSTRACT>", evt)

      
  }

}
