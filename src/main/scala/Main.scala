
import akka.actor.{ ActorSystem, Props }
import org.asteriskjava.manager.ManagerConnectionFactory
import scala.io.StdIn

object Main {

  def main(args: Array[String]): Unit = {

    implicit val system = ActorSystem("System")

    println(s"\u001B[32mStarting, Ctrl+D to stop and go back to the console...\u001B[0m")

    val cnx = new ManagerConnectionFactory(
      "192.168.56.2",
      5038,
      "xuc",
      "xucpass"
    )

    system.actorOf(Props(new AmiConnector(cnx)))

    while(StdIn.readLine() != null) {}
    system.terminate()
    ()
  }
}

