import BaseGraph._

object AsteriskGraph extends GraphBuilder[AsteriskObject, AsteriskGraph]{
  implicit def newBuilder(links: Links[AsteriskObject]): AsteriskGraph = new AsteriskGraph(links)
}

class AsteriskGraph(val links: Links[AsteriskObject]) extends BaseGraph[AsteriskObject, AsteriskGraph](links) {

  /**
    *  /!\ DO NOT USE EXCEPT FOR TESTING
    *      Very Bad Code follows...
    */
  def prettyPrint(): String = {
    links.collect({ case (c:Channel, _) => c }).toList
      .sortBy(_.name)
      .foldLeft[(Set[AsteriskObject], List[String])]((Set.empty, List.empty))( (pair, channel) => {
        val res = printNode(channel, pair._1)
        (pair._1 ++ res._1, pair._2 ++ res._2)
      })._2
      .mkString("\n")
  }

  private def printNode(parent: AsteriskObject, visited: Set[AsteriskObject]): (Set[AsteriskObject], List[String]) = {
    if(visited.contains(parent)) {
      (visited, List.empty)
    } else {
      val children = (neighbors(parent) -- visited).toList.sortBy(_.name)
      val newVisited = visited + parent
      val parentStr = parent.prettyPrint
      val prefix = " ".*(parentStr.length())

      if(children.size == 0) {
        (newVisited, List(s"$parentStr"))
      } else if(children.size == 1) {
        val pair = printNode(children.head, newVisited)
        (pair._1, List(s"$parentStr ─ " + pair._2.head) ++ pair._2.tail.map(l => s"$prefix   $l"))
      } else {
        val pair = printNode(children.head, newVisited)

        val head = List(s"$parentStr ┬ " + pair._2.head) ++ pair._2.tail.map(l => s"$prefix │ $l")
        val childPair = children.tail.foldLeft[(Set[AsteriskObject], List[String])]((pair._1, List.empty))( (pair, channel) => {

          val res = printNode(channel, pair._1)
          (pair._1 ++ res._1, pair._2 ++ res._2)
        })
        val childStrings = childPair._2.dropRight(1).map(l => s"$prefix ├ $l") :+( s"$prefix └ " + childPair._2.last)
        (childPair._1, head ++ childStrings)
      }
    }
  }



  def remoteChannels(channel: Channel): Set[Channel] = connectionsOf(channel).collect({ case c:Channel => c})

}




