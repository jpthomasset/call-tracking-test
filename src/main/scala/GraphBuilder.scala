import BaseGraph._

sealed trait EdgeBuilder[Node] {
  def build[T <: BaseGraph[Node, T]](graph: T): T
}

final class NodeList[Node](val nodes: List[Node]) extends EdgeBuilder[Node]{
  @inline def ~[NodeB>:Node](y: NodeB): NodeList[NodeB] = new NodeList(y :: nodes)

  def build[T <: BaseGraph[Node, T]](graph: T): T = {
    toEdges().foldRight[T](graph)((edge, g) => {
      g.add(edge)
    })
  }

  def toEdges[NodeB>:Node]() : List[Edge[NodeB]] = {
    nodes.foldRight[(List[Edge[NodeB]], Option[Node])]((List.empty, Option.empty))((node, pair) => {
      if(pair._2.isEmpty) {
        (pair._1, Some(node))
      } else {
        (Edge[NodeB](pair._2.get, node) :: pair._1, Some(node))
      }
    })._1
  }
}


abstract class GraphBuilder[Node, T <: BaseGraph[Node, T]] {

  def newBuilder(links: Links[Node]): T

  def apply(): T = newBuilder(Map.empty)

  def apply(lists: NodeList[Node]*) : T =
    add(apply(), lists:_*)

  def add(graph: T, lists: NodeList[Node]*) : T =
    lists.foldRight[T](graph)((nodes, g) => nodes.build[T](g))

  def del(graph: T, lists: NodeList[Node]*) : T = 
    lists.flatMap(_.toEdges()).foldRight(graph)((edge, g) => g.del(edge))

}

object GraphBuilder {

  implicit final class NodeWrapper[Node](private val self: Node) extends EdgeBuilder[Node] {
    @inline def ~[NodeB>:Node](y: NodeB): NodeList[NodeB] = new NodeList(y :: self :: Nil)

    def build[T <: BaseGraph[Node, T]](graph: T): T = throw new Exception("Incomplete Graph")
  }

  implicit def toEdges[Node, NodeB >: Node](list: NodeList[Node]) : List[Edge[NodeB]] = list.toEdges[NodeB]()

  def apply[Node, T <: BaseGraph[Node, T]](graph: T)(list: NodeList[Node]): T =
    list.build[T](graph)
}

