sealed trait AsteriskObject {
  def name: String
  def prettyPrint: String = toString
}

sealed trait ChannelLike extends AsteriskObject
case class Channel(name: String) extends ChannelLike
case class LocalChannel(name: String) extends ChannelLike

sealed trait BridgeLike extends AsteriskObject
case class Bridge(name: String) extends BridgeLike
case class LocalBridge(lc1: LocalChannel, lc2: LocalChannel) extends BridgeLike {
  override def name = s"$lc1#$lc2"
  override def prettyPrint = "LocalBridge(...)"
}
case class Dial(c1: ChannelLike, c2: ChannelLike) extends BridgeLike {
  override def name = s"${c1.name}#${c2.name}"
  override def prettyPrint = "Dial(...)"
}

case class WeakBridge(name: String) extends BridgeLike {
  override def prettyPrint = s"WeakBridge($name)"
}

object WeakBridge {
  def apply(c1: ChannelLike, c2: ChannelLike): WeakBridge =
    WeakBridge(List(c1.name, c2.name).sorted.mkString("#"))
}

sealed trait AsteriskDevice extends AsteriskObject
case class SipDevice(name: String) extends AsteriskDevice
case class SccpDevice(name: String) extends AsteriskDevice
case class CustomDevice(name: String) extends AsteriskDevice

sealed trait AsteriskTrunk extends AsteriskObject
case class SipTrunk(name: String) extends AsteriskTrunk
case class IaxTrunk(name: String) extends AsteriskTrunk
case class DahdiTrunk(name: String) extends AsteriskTrunk


object Channel {
  def from(name: String) =
    Option(name).map(_.startsWith("Local/")) match {
      case Some(true) => LocalChannel(name)
      case Some(false) => Channel(name)
      case None => Channel("null")
    }
}
