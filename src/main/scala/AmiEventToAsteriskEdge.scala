import org.asteriskjava.manager.event._

import GraphBuilder._

object AmiEventToAsteriskEdge {

  def apply(graph: AsteriskGraph, event: ManagerEvent): AsteriskGraph = event match {

    case evt: HangupEvent =>
      graph.del(Channel.from(evt.getChannel()))

    case evt: DialBeginEvent =>
      val c1 = Channel.from(evt.getChannel())
      val c2 = Channel.from(evt.getDestination())

      AsteriskGraph.add(graph, c1 ~ Dial(c1, c2) ~ c2)

    case evt: DialEndEvent =>
      val c1 = Channel.from(evt.getChannel())
      val c2 = Channel.from(evt.getDestination())

      graph.del(Dial(c1, c2))

    case evt: LocalBridgeEvent =>
      val lc1 = LocalChannel(evt.getLocalOneChannel())
      val lc2 = LocalChannel(evt.getLocalTwoChannel())

      AsteriskGraph.add(graph, lc1 ~ LocalBridge(lc1, lc2) ~ lc2)

    case evt: BridgeEnterEvent =>
      AsteriskGraph.add(graph, Channel.from(evt.getChannel) ~ Bridge(evt.getBridgeUniqueId))

    case evt: BridgeLeaveEvent =>
      AsteriskGraph.del(graph, Channel.from(evt.getChannel) ~ Bridge(evt.getBridgeUniqueId))

    case _ => graph
  }
}
